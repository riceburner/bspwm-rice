#!/usr/bin/env bash

# my lemonbar panel
# license: Simplified BSD License (2-clause)
# inspired/borrowed from simplepanel mostly (https://github.com/kainonergon/simplepanel)


###[ errors, traps, and cleanups ]###

# If a command fails or exit status greater than 1, exit
set -e
# Abort startup if another instance was found    
if [[ "$(pgrep -x lemonbar | wc -l)" -gt 1  ]]; then
  echo 'Sorry. Only one instance allowed.'
  killall lemonbar
  exit 0
fi
# trap to clean up bspwm top_padding if lemonbarpanel exits
trap "bspc config top_padding 0" EXIT INT QUIT TERM
# set bspwm top_padding when lemonbarpanel starts
bspc config top_padding 15


###[ colors (gruvbox) ]###

color_fg='%{F#EBDBB2}'
color_gray='%{F#A89984}'
color_gray_hi='%{F#928374}'
color_blue='%{F#458588}'
color_blue_hi='%{F#83A598}'
color_yellow='%{F#D79921}'
color_yellow_hi='%{F#FABD2F}'
color_green='%{F#689D6A}'
color_green_hi='%{F#B8BB26}'
color_aqua='%{F#689D6A}'
color_aqua_hi='%{F#8EC07C}'
color_red='%{F#CC241D}'
color_red_hi='%{F#FB4934}'
color_reset='%{F-}'


###[ ascii chars ]###

hor_1_4='▏'
hor_2_4='▎'
hor_3_4='▍'
hor_4_5='▌'
hor_5_5='▉'
ac_plug='¥'
no_batt=' '
perc_90='█'
perc_80='█'
perc_70='▇'
perc_60='▆'
perc_50='▅'
perc_40='▄'
perc_30='▃'
perc_20='▂'
perc_10='▁'
vol_100='─────────┤'
vol_90='────────┼'
vol_80='───────┼─'
vol_70='──────┼──'
vol_60='─────┼───'
vol_50='────┼────'
vol_40='───┼─────'
vol_30='──┼──────'
vol_20='─┼───────'
vol_10='┼────────'
vol_mu='_________'
wifi_4_bars='▂▄▆█'
wifi_3_bars='▂▄▆_'
wifi_2_bars='▂▄__'
wifi_1_bars='▂___'
wifi_0_bars='____'


### [ loop ] ###

# Print the statusline
round=0
statusline(){
	echo -n "${round}"
#	echo -n "${round} %{l} ${status_desktops} %{c} %{r} ${status_mem} ${status_vol} ${status_wifi} ${status_batt} ${status_datetime} "
}
every() {
  [ $((round % ${1:-1})) -eq 0 ]
}
while true; do
  statusline
  let round++
  sleep 0.1
done
#while sleep 0.1; do
# #status_desktops
#case "${round}" in
#  1)
#    cur="$(xprop -root _NET_CURRENT_DESKTOP | awk '{print $3+1}')"
#    tot="$(xprop -root _NET_NUMBER_OF_DESKTOPS | awk '{print $3}')"
#    for ((i=1;i<=tot;i++)); do
#      if [[ ${i} -ne ${cur} ]]; then
#        status_desktops=${status_desktops}"${color_blue} [%{O4}${i}%{O4}] ${color_reset}"
#      else
#        status_desktops=${status_desktops}"${color_blue_hi}╒┤${color_reset}%{+o}%{O4}${color_fg}${i}${color_reset}%{O4}%{-o}${color_blue_hi}├╕${color_reset}"
#      fi
#    done
#    ;;
#  5)
# #status_mem
#    mem_used=$(free -m | awk 'FNR == 2 {print $3}')
#    mem_total=$(free -m | awk 'FNR == 2 {print $2}')
#    status_mem=" mem: ${color_gray}${mem_used}${color_reset}/${mem_total} MiB "
# #status_vol
#    volume_lvl=$(amixer -c 0 get Headphone \
#	         | sed -n 's/^.*\[\([0-9]\+\)%.*$/\1/p'  \
#	         | head -n 1)
#    volume_muted=$(amixer -c 0 get Headphone \
#      	            | sed -n 's/^.*\[\(o[nf]\+\).*$/\1/p' \
#		    | head -n 1)
#    if [[ ${volume_muted} = 'off'  ]]; then
#      icon="${vol_mu}"
#    else
#      case $(( volume_lvl / 10 )) in
#        0) icon="${vol_10}" ;;
#        1) icon="${vol_10}" ;;
#        2) icon="${vol_20}" ;;
#        3) icon="${vol_30}" ;;
#        4) icon="${vol_40}" ;;
#        5) icon="${vol_50}" ;;
#        6) icon="${vol_60}" ;;
#        7) icon="${vol_70}" ;;
#        8) icon="${vol_80}" ;;
#        9) icon="${vol_90}" ;;
#        *) icon="${vol_100}" ;;
#      esac
#    fi
#    status_vol="vol: ${color_gray}${icon}${color_reset} "
#    ;;
#  20)
# #status_wifi
#    if grep -sq 'up' /sys/class/net/wlp2s0/operstate; then
#      wifi_quality=$(awk 'NR==3 {print int($3 * 100 / 70)}' /proc/net/wireless)
#      case $(( ${wifi_quality} / 20 )) in
#        5|4)  label=${color_green}${wifi_4_bars}${color_reset} ;;
#        3)    label=${color_green}${wifi_3_bars}${color_reset} ;;
#        2)    label=${color_green}${wifi_2_bars}${color_reset} ;;
#        1)    label=${color_yellow}${wifi_1_bars}${color_reset} ;;
#        0)    label=${color_red}${wifi_0_bars}${color_reset} ;;
#        *)    label='wathappen' ;;
#      esac
#      wifi_name="$(iwgetid -r)"
#      status_wifi="${wifi_name} ${label}"
#    else
#      status_wifi="wifi: NONE"
#    fi
#    ;;
#  100)
#    #status_batt
#    read -r bat_cap < /sys/class/power_supply/BAT0/capacity
#    read -r ac_online < /sys/class/power_supply/AC/online
#    bat_stat="$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 \
#                | awk 'FNR == 12 {print $2}')"
#    rem_time_bat=$(acpi -b | awk '{print substr($5, 1, length($5)-3)}')
#    notification_status=0
#    charging_notified=1
#    case $(( ${bat_cap} / 10 )) in
#      0) icon="${color_red}${perc_10}${color_reset}" ;;
#      1) icon="${color_yellow}${perc_10}${color_reset}" ;;
#      2) icon="${color_green}${perc_20}${color_reset}" ;;
#      3) icon="${color_green}${perc_30}${color_reset}" ;;
#      4) icon="${color_green}${perc_40}${color_reset}" ;;
#      5) icon="${color_green}${perc_50}${color_reset}" ;;
#      6) icon="${color_green}${perc_60}${color_reset}" ;;
#      7) icon="${color_green}${perc_70}${color_reset}" ;;
#      8) icon="${color_green}${perc_80}${color_reset}" ;;
#      9) icon="${color_green}${perc_90}${color_reset}" ;;
#      *) icon="${color_green}${perc_90}${color_reset}" ;;
#    esac
#    if [[ ${ac_online} -eq 0 ]]; then
#      status_batt=" discharging ${color_gray}${rem_time_bat}${color_reset} ${icon}   "
#      if [[ ${bat_cap} -gt 20 ]]; then
#      notification_status=0
#      elif [[ ${bat_cap} -le 20 && ${bat_cap} -gt 10 && ${notification_status} -eq 0 ]]; then
#        notify-send "Warning! Battery percentage has dropped to 20%"
#        # dunst keeps notifying...
#        notification_status=1
#      elif [[ ${bat_cap} -le 10 && ${bat_cap} -gt 5 && ${notification_status} -eq 1 ]]; then
#        notify-send "Warning! Battery percentage has dropped to 10%"
#        notification_status=2
#        #isn't getting to here... because of variable not carrying back? 
#      elif [[ ${bat_cap} -le 5 && ${notification_status} -eq 2 ]]; then
#        notify-send -u critical "Warning! Battery (<5%) is nearly DIE!!!"
#      fi
#    else
#      if [[ ${bat_stat} = 'fully-charged' ]]; then
#        status_batt=" CHARGED ${color_green} ${ac_plug} ${color_reset} "
#      else
#        status_batt=" charging ${color_blue}${ac_plug}${color_reset} ${rem_time_bat} ${icon} "
#      fi
#    fi
#    ;;
#  9)
#    status_datetime=$(date "+ ${color_gray}%F${color_reset} ${color_blue}[ ${color_reset}${color_fg}%H:%M${color_reset}${color_blue} ]${color_reset}")
#    ;;
#  *) ;;
#esac
#  statusline
#  let round++
#  #(( round++ ))
#done
