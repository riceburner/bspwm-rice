#!/bin/ksh

xset +fp /usr/local/share/fonts/terminus
xset fp rehash

bgcol='#3C3836'
fgcol='#689d6a'
font='-xos4-terminus-medium-r-normal--14-140-72-72-c-80-iso10646-1'

sh /home/puffy/scrp/lemonpanel/lemonbarpanel | 
	lemonbar -p -g "1366x13+0+0" \
	-F "$fgcol" -B "$bgcol" -f "$font"
